''' @file MotorDriver.py 
  Defines a Motor Driver class that utilizes PWM

  This file contains a definition for the Motor Driver class and a main method
  to test it. Implemented in the MotorDriver class are the methods enable(), 
  disable(), and set_duty(duty).

  @package MotorDriver
  Encapsulates features associated  with  driving  a  DC  motor  connected  to 
  IHM40A1 motor  driver  carrier  board  and Nucleo.



  @date Fri Apr 24 09:43:10 2020

  @author: James Popolow '''

import pyb

class MotorDriver:
    ''' This class implements a motor driver for the ME405 board. '''
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on IN1_pin
            and IN2_pin. '''
        print ('Creating a motor driver')
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.disable()
          
    def enable (self):
        ''' Enables the motor by setting the ENa pin high '''
        print ('Enabling Motor')
        self.EN_pin.high()
              
    def disable (self):
        ''' Disables the motor by setting the ENa pin low '''
        print ('Disabling Motor')
        self.EN_pin.low()
                  
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
            cycle of the PWM signal sent to the motor.
            Valid range: -100 to 100'''
            
        # Check that the given duty cycle is valid
        if duty < -100 or duty > 100:
            raise ValueError
            
        # If duty cycle is positive, configure pins and timer channels to
        # move 'forwards'
        if duty >= 0:
            self.IN1_pin.low()
            timerChannel = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
            timerChannel.pulse_width_percent(duty)
        # Otherwise, configure everything to move 'backwards'
        else:
            self.IN2_pin.low()
            timerChannel = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
            timerChannel.pulse_width_percent(-duty)
                  
if __name__ =='__main__':
    # The following code is a test program for the motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN  = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
    
    # Create the timer object used for PWM generation
    tim     = pyb.Timer(3, freq=20000);
    
    # Create a motor object passing in the pins and timer
    moe     = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 20 percent, for 1 second, the -50% for one second.
    moe.set_duty(20)
    pyb.delay(1000)
    moe.set_duty(-50)
    pyb.delay(1000)
    
    # Disable the motor
    moe.disable()