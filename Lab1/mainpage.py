## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 1 is an introduction to programming with MycroPython and interacting with our hardware.
#  Our task was to write a motor driver that would implement all of the functionality
#  necessary to interact with our motors using Pulse Width Modulation (PWM). The most important information
# about the lab is provided below, but the full lab handout can be found at:
# https://drive.google.com/open?id=1a-ZYUUm6j1APHosPGhv3HHjLI1SaOcRJ
# 
#  @section Background
#  DC motors are simple reliable actuators that are ubiquitous in the world around us.
#  In practice most motors are controlled using switching circuitry and PWM because it is cheaper, more
#  efficient, and simpler than using analog circuitry. PWM motor control techniques take advantage of the
#  fact that DC Motors act like low-pass filters. Therefore, high frequency input signals are filtered out
#  but the low frequency average value of the signal passes through.  If the frequency of this PWM signal is
#  large enough, then the motor will only be effected by the average value. Consider a square waveform. About
#  halfway through the waveform,  the duty-cycle changes from about 40% to about 80%. From the
#  motor’s ‘perspective’ this is equivalent to the applied voltage across the motor leads changing from V= 0.4V
#  toV= 0.8V, doubling the effort requested from the motor. 
# 
#  @section Assignment
#  Your assignment for this week is to write a motor driver class that encapsulates all of the features
#  associated  with  driving  a  DC  motor  connected  to  the IHM40A1 motor  driver  carrier  board  that
#  plugs into the top of the Nucleo. The IHM04A1 carries the L6206 motor driver from ST Microelectronics.
#  One of the chip’s two H-bridges consists of four N-channel FET’s, with the motorconnected to pins OUT1A and OUT2A.
#  The microcontroller controls pins ENA, IN1A, andIN2A. A common way of using these pins is to set ENA high to enable
#  the motor, set IN1A low, and send a PWM signal to IN2A to power the motor in one direction; reverse the signals to
#  IN1A and IN2A to power the motor in the other direction.
#
#  @section Code
#  Code for this lab can be found at https://bitbucket.org/jpopolow/me405/src/master/Lab1/
# 
#  @author James Popolow
#
#  @copyright License Info
#
#  @date April 24, 2020
#