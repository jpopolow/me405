## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage Lab 0
#
#  @section sec_intro Introduction
#  Lab 0 is an introduction to python programming and consisted of writing a program which would
#  calculate a fibonacci number corresponding to an index provided by user input.
#
#  @section sec_Fib Fibonacci
#  Some information about Fibonacci 
#
#  @author James Popolow
#
#  @copyright License Info
#
#  @date 4/24/20
#