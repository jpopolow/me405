''' @file file.py
  
    @package Encoder

    @subsection purpose Purpose
    
    
    @subsection background Background
    
  
    @subsection impl Implementation Strategy
    
  
    @subsection usage Usage
    
    
    @subsection testing Testing
    
  
    @subsection bugs_and_lims Bugs and Limitations
    
  
    @subsection source Source Code
    
  
    @author James Popolow

    @date May 6, 2020 '''

import pyb

class whatever:
    ''' description

    @b Example
    @code
    if __name__ =='__main__':
        # The following code is a test program for the encoder class. Any
        # code within the if __name__ == '__main__' block will only run when the
        # script is executed as a standalone program. If the script is imported as
        # a module the code block will not run.
    
        # Test code 
    @endcode 

    Source code https://bitbucket.org/jpopolow/me405/src...

    @author James Popolow
    @date May 6, 2020 '''