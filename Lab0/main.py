## @file main.py
#  This file is a simple program that prompts the user for a Fibonacci sequence
#  index, validates it, then calculates and prints the value at that index.
#
#  @author James Popolow
#
#  @date 4/17/20

## Collects input from user
#
#  This method prompts the user for a Fibonacci index, validates it,
#  finds the value at that index, prints the value, and repeats if requested
def fib ():
    # Prompt user for an index
    index = input('Input a Fobonacci index (non-negative integer): ')
    
    # Validate the index, then calculate and print the corresponding value if valid
    if (validateFibIndex(index)) : 
        print('Calculating Fibonacci number at index n = {:}.'.format(index))
        print('The Fibonacci number at that index is: {:}'.format(fibrec(int(index))))    
        
    # Ask to repeat
    reply = input('Would you find another Fibonacci number? (y/n) ')
    
    # Repeat if reply was 'y'
    if (reply == 'y') :
        fib()
    
## Recursively calculate Fibonacci value at given index
#
#  This method calculates a Fibonacci number corresponding to
#  a specified index.
#
#  @param index An integer specifying the index of the desired Fibonacci num.  
def fibrec (index):
    # Base case 1: f(0) = 0    
    if index == 0:      
        return 0
    # Base case 2: f(1) = 1
    elif index == 1:    
        return 1
    # Fib sequence formula
    else:
        return fibrec(index - 1) + fibrec(index - 2)    
    
## Validates given Fibonacci index
#
#  This method validates a fibonacci index given as a string,
#  returning 1 if valid and 0 if invalid.
#
#  @param index A string representing an endex in the Fibonacci sequence.  
def validateFibIndex (index):
    try:
        # Test string to int conversion and integer value
        if int(index) >= 0:     
            #If valid, return 1
            return 1            
    except:
        # If cannot convert to integer, continue
        pass                    
        
    # Tell the user something is wrong, then return 0
    print('{:} is not a valid index'.format(index))
    return 0

    
if __name__ == '__main__':
    # Call fib()
    fib()
    