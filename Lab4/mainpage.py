''' @file mainpage.py
  @author James Popolow
  @mainpage

  @section intro Introduction
  This website documents the code produced for ME405. 
  
  @section labs Labs
  \li Lab 1 https://bitbucket.org/jpopolow/me405/src/master/Lab1/
  \li Lab 2 https://bitbucket.org/jpopolow/me405/src/master/Lab2/
  \li Lab 3 https://bitbucket.org/jpopolow/me405/src/master/Lab3/
  \li Lab 4 https://bitbucket.org/jpopolow/me405/src/master/Lab4/

  
  @section drivers Drivers and Other Files
  \li MotorDriver
  \li Encoder
  \li IMUDriver
  \li CLProportionalController
  \li CLProportionalControllerTest
 
  @section Code
  Code for this class can be found at https://bitbucket.org/jpopolow/me405/src/master/ \n
  (Links to more specific directories and source files can be found in corresponding location deepin gin the documentation)
 
  @author James Popolow

  @copyright License Info

  @date April 24, 2020 '''