''' @file CLProportionalControllerTest.py

    @package CLProportionalControllerTest
    
    Makes use of the @ref CLProportionalController.
    
    For an example of using this program to tune a proportional gain, see \ref CLPCStepResponsePage

    Code:
    
    @include CLProportionalControllerTest.py
    
    Or at https://bitbucket.org/jpopolow/me405/src/master/Lab3/CLProportionalControllerTest.py
    '''

import pyb
import utime
from MotorDriver import MotorDriver
from Encoder import Encoder
from CLProportionalController import CLProportionalController

if __name__ =='__main__':
    # The following code is a test program for the CLProportionalController class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
    
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq=20000);
    
    # Create a motor object passing in the pins and timer
    motor = MotorDriver(pin_EN, pin_IN1, pin_IN2, timer)
    
    # Enable the motor driver
    motor.enable()
    
    # Create the pin objects used for interfacing with the encoder driver
    encoderPin1 = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
    encoderPin2 = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
    
    # Create an encoder object passing in the pins and corresponding timer (4)
    encoder = Encoder(encoderPin1, encoderPin2, 4)
    
    # Set Kp (%duty / encoder count)
    kp = 0.15
    
    # Create closed loop proportional controller passing in
    controller = CLProportionalController(kp)
    # motor.set_duty(20)
    
    # Set up step response test parameters
    stepInitialValue = setpoint = 0
    stepFinalValue = 1500
    time = 3 # seconds
    stepTime = 1 # second
    updatePeriod = 0.01 #seconds
    loops = time / updatePeriod # number of loops
    count = 0
    
    # Set up lists for data collection
    time = []
    desired = []
    position = []
    duty = []
    
    # Record test start time
    startTime = utime.ticks_ms()
    
    # Perform step response
    print("Performing step response test")
    while count < loops:
        count = count + 1
        # Change setpoint to 15000 at 1 seconds
        if count == (stepTime / updatePeriod):
            setpoint = stepFinalValue
        time.append((utime.ticks_ms() - startTime) / 1000)
        desired.append(setpoint)
        position.append(encoder.get_position())
        encoder.update()
        newDuty = controller.update(setpoint, encoder.get_position())
        duty.append(newDuty)
        motor.set_duty(newDuty)
        utime.sleep_ms(updatePeriod * 1000)
    print("Step response test complete")
    
    # Disable motor
    motor.disable()
                        
    # Print results
    print("Printing results:")
    for i in range(len(time)):
        print("{}, {}, {}, {}".format(time[i], desired[i], position[i], duty[i]))
    
