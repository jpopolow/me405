''' @file mainpage.py
  @author James Popolow
  @mainpage

  @section intro Introduction
  This website documents the code produced for ME405. 
  
  @section labs Labs
  Lab 1 https://bitbucket.org/jpopolow/me405/src/master/Lab1/ \n
  Lab 2 https://bitbucket.org/jpopolow/me405/src/master/Lab2/ \n
  
  @section Drivers
  MotorDriver \n
  Encoder \n
 
  @section Code
  Code for this class can be found at https://bitbucket.org/jpopolow/me405/src/master/ \n
  (Links to more specific directories and source files can be found in corresponding location deepin gin the documentation)
 
  @author James Popolow

  @copyright License Info

  @date April 24, 2020 '''