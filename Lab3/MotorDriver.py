''' @file MotorDriver.py 
  Defines a Motor Driver class that utilizes PWM

  This file contains a definition for the Motor Driver class and a main method
  to test it. Implemented in the MotorDriver class are the methods enable(), 
  disable(), and set_duty(duty).

  @package MotorDriver
  Implements interaction of DC motor and IHM40A1 motor driver carrier
  
  @subsection purpose Purpose
  This motor driver class that encapsulates all of the features
  associated  with  driving  a  DC  motor  connected  to  the IHM40A1 motor  driver  carrier  board  that
  plugs into the top of the Nucleo.
  
  @subsection background Background
  DC motors are simple reliable actuators that are ubiquitous in the world around us.
  In practice most motors are controlled using switching circuitry and PWM because it is cheaper, more
  efficient, and simpler than using analog circuitry. PWM motor control techniques take advantage of the
  fact that DC Motors act like low-pass filters. Therefore, high frequency input signals are filtered out
  but the low frequency average value of the signal passes through.  If the frequency of this PWM signal is
  large enough, then the motor will only be effected by the average value. Consider a square waveform. About
  halfway through the waveform,  the duty-cycle changes from about 40% to about 80%. From the
  motor’s ‘perspective’ this is equivalent to the applied voltage across the motor leads changing from V= 0.4V
  toV= 0.8V, doubling the effort requested from the motor. ;
  
  @subsection impl Implementation Strategy
  The IHM04A1 carries the L6206 motor driver from ST Microelectronics.
  One of the chip’s two H-bridges consists of four N-channel FET’s, with the motorconnected to pins OUT1A and OUT2A.
  The microcontroller controls pins ENA, IN1A, andIN2A. A common way of using these pins is to set ENA high to enable
  the motor, set IN1A low, and send a PWM signal to IN2A to power the motor in one direction; reverse the signals to
  IN1A and IN2A to power the motor in the other direction.
  
  @subsection usage Usage
  Usage
  
  @subsection testing Testing
  Testing
  
  @subsection bugs_and_lims Bugs and Limitations
  Bugs and Limitations
  
  @subsection source Source Code
  https://bitbucket.org/jpopolow/me405/src/master/Lab2/MotorDriver.py

  @date Fri Apr 24 09:43:10 2020

  @author James Popolow '''
import pyb

class MotorDriver:
    ''' This class implements a motor driver for the ME405 board. '''
    def __init__ (self, EN_pin, INA_pin, INB_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on IN1_pin
            and IN2_pin. '''
        print ('Creating a motor driver')
        self.EN_pin = EN_pin
        self.IN1_pin = INA_pin
        self.IN2_pin = INB_pin
        self.timer = timer
        self.tch1 = timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.tch2 = timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        self.disable()
          
    def enable (self):
        ''' Enables the motor by setting the ENable pin high '''
        print ('Enabling Motor')
        self.EN_pin.high()
              
    def disable (self):
        ''' Disables the motor by setting the ENa pin low '''
        print ('Disabling Motor')
        self.EN_pin.low()
                  
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
            cycle of the PWM signal sent to the motor.
            Valid range: -100 to 100'''
            
        # Check that the given duty cycle is valid
        if duty < -100:
            print("Given invalid value of {}, using -100".format(duty))
            duty = -100
        elif duty > 100:
            print("Given invalid value of {}, using 100".format(duty))
            duty = 100
            
        # If duty cycle is positive, configure pins and timer channels to
        # move 'forwards'
        if duty >= 0:
            self.tch1.pulse_width_percent(duty)
            self.IN2_pin.low()
        # Otherwise, configure everything to move 'backwards'
        else:
            self.IN1_pin.low()
            self.tch2.pulse_width_percent(-duty)
                  
if __name__ =='__main__':
    # The following code is a test program for the motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
    
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq=20000);
    
    # Create a motor object passing in the pins and timer
    motor = MotorDriver(pin_EN, pin_IN2, pin_IN1, timer)
    
    # Enable the motor driver
    motor.enable()
    
    # Set the duty cycle to 20 percent, for 1 second, the -50% for one second.
    motor.set_duty(20)
    pyb.delay(1000)
    motor.set_duty(-50)
    pyb.delay(1000)
    
    # Disable the motor
    motor.disable()
    