''' @file Encoder.py
  
    @package Encoder

    @subsection purpose Purpose
    This motor driver class encapsulates all of the features
    associated  with  driving  a  DC  motor  connected  to  the IHM40A1 motor  driver  carrier  board  that
    plugs into the top of the Nucleo.
  
    @subsection background Background
    (From the Lab 1 handout) \n
    A common way to measure the movement of a motor is with an optical encoder.  Because most encoders can send
    out signals at high frequency (sometimes up to 1MHz or so), we can’t read one with regular code; we need
    something faster, such as interrupts or dedicated hardware. Our STM32 processors have such hardware timers
    which can read pulses from encoders and count distance and direction of motion. Incremental encoders send
    digital signals over two wires, usually in quadrature, the two signals are about 90 degrees out of phase.
        
    Each time the state of either encoder channel, A or B, changes, it is an indication that the encoder has moved
    one tick. The particular state change tells the direction in which the encoder has moved. For example, if
    the state (A, B) starts at (0, 0), then a change to (0, 1) indicates movement in one direction, while (0, 1) to
    (0, 0) indicates movement in the other direction.
  
    @subsection impl Implementation Strategy
    We can take advantage of the timer and it's channels to keep track of the position of the motor.
    For each tick of the encoder we can increment the timer counter either up or down depending on the
    direction of rotation and keep track of the position this way. However, when the timer counter tries
    to surpass its maximum value (35535 for a 16 bit counter) it will reset to 0 and start counting all
    over again. This is called overflow. Likewise, if the counter tries to go below 0 it will wrap
    around to 35535 again. This is known as underflow. \n \n
    
    A  way  to  solve  the  problem  is  to read  the  timer  rapidly  and  repeatedly,  and  each  time
    compute  the  change,  or delta,  in timer count since the previous update(), and add the distance to
    a variable which holds the position.  This position should count total movement and not reset for each
    revolution or when the timer overflows.
  
    @subsection usage Usage
    To initialize an Encoder object, simply supply its constructor with two pins that correspond with channel
    1 and channel 2 of a timer module as well as the id (an integer) of that timer. \n
    @code{.py}
    encoder = Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
    @endcode
    
    Then, you can call methods on it. Remember that @code{.py} update() @endcode must be called rapidly and
    continuously during motor operation in order to maintain an accurate position reading. \n \n
    
    You can also call @code{.py} set_position() @endcode and @code{.py} get_position() @endcode on the encoder.
    For example:
    
    @code{.py}
    encoder.set_position(0)
    position = encoder.get_position()
    print(position)
    @endcode
    
    will print 0 to the console assuming the motor has not rotated.
    
    @subsection testing Testing
    This class has been tested using the B6 and B7 pins and Timer 4 on the  Nucleo. Calling update() within
    a while loop and subsequestly printing the result of get_position() confirmed functionality.
  
    @subsection bugs_and_lims Bugs and Limitations
    This encoder class has only been tested for the period and prescaler to which the timer is configured in
    the class constructor. Other configurations may produce different results. \n \n
    
    Also, while the position is tracked ignoring the timer counter overflow/underflow, the variable that position
    is stored in does have a minimum/maximum value, and after continued rotation in one direction it may reach this
    value. Even if the code continued to run, the position would almost certainly be inaccurate. \n \n
    
    Finally, the two pins provided to the encoder object MUST correspond to channel 1 and channel 2 of the corresponding
    given timer. Otherwise the counter will not work correctly.
  
    @subsection source Source Code
    https://bitbucket.org/jpopolow/me405/src/master/Lab2/Encoder.py
  
    @author James Popolow

    @date May 6, 2020 '''

import pyb

class Encoder:
    ''' Encapsulates the operation of the timer to read from an encoder connected to arbitrary pins.

    @b Example
    @code
    if __name__ =='__main__':
        # The following code is a test program for the encoder class. Any
        # code within the if __name__ == '__main__' block will only run when the
        # script is executed as a standalone program. If the script is imported as
        # a module the code block will not run.
    
        # Create the pin objects used for interfacing with the encoder driver
        pB6 = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
        pB7 = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
    
        # Create an encoder object passing in the pins and corresponding timer (4)
        encoder = Encoder(pB6, pB7, 4)
        
        count = 0
        while 1:
            count = count + 1
            encoder.update()
            
            # Only print position every 1000 times through the loop
            if count % 1000 == 0:
                print(encoder.get_position())
    @endcode 

    Source code https://bitbucket.org/jpopolow/me405/src/master/Lab2/Encoder.py

    @author James Popolow
    @date May 6, 2020 '''
  

    ## Constructor for encoder driver
    #
    #  This constructor takes two pins and an integer representing the corresponding timer module.
    #  The pin MUST correspond to channel 1 and channel 2 of the given timer. The constructor configures
    #  the given pins to INPUT mode. The timer prescaler is set to 0 and the period to 0xFFFF in order to
    #  maximize the resolution and range of the 16 bit timer. The timer count is incremented (or decremented)
    #  each tick of the encoder until reaching 65535 at which point it resets to 0 \n \n
    #  Example usage: Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
    #
    #  @param pinA First of two pins, corresponding to encoder channel A (e.g. pyb.Pin.cpu.B6)
    #  @param pinB Second pin, corresponding to encoder channel B (e.g. pyb.Pin.cpu.B7)
    #  @param timerNum An integer representing the timer module corresponding to given pins (e.g. 4)
    #  @return An initialized Encoder object
    def __init__(self, pinA, pinB, timerNum):
        print("Setting up an encoder")
        # Configure pins to input mode
        ## Pin object corresponding to channel 1 of given timer
        self.pA = pyb.Pin(pinA, pyb.Pin.IN)
        ## Pin object corresponding to channel 2 of given timer
        self.pB = pyb.Pin(pinB, pyb.Pin.IN)
        
        # Configure timer to maximize range and resolution
        ## Dual channel timer whose counter is incremented by the encoder
        self.timer = pyb.Timer(timerNum, prescaler=0, period=0xFFFF)
        ## Channel one of the timer object, configured in mode ENC_A for pin pA
        self.ch1 = self.timer.channel(1, pyb.Timer.ENC_A, pin=self.pA)
        ## Chanel two of the timer object, configured in mode ENC_B for pin pB
        self.ch2 = self.timer.channel(2, pyb.Timer.ENC_B, pin=self.pB)
        ## Represents total absolute rotation of encoder
        print("Setting encoder position to 0")
        self.position = 0
        ## Value of the timer count last time update() was run
        self.lastTimerCount = 0
        
        
    ## Updates the recorded position of the encoder
    #
    #  Takes the last known position of the encoder and the current position and adds the difference,
    #  either positive or negative, to the last known position of the motor, thereby updating the current
    #  absolute position of the motor. This method must be called frequently during motor operation
    #  in order to maintain accuracy.
    #
    #  @return void
    def update(self):
        currentTimerCount = self.timer.counter()
        newPosition = self.position + self.get_delta(self.lastTimerCount, currentTimerCount)
        self.set_position(newPosition)
        self.lastTimerCount = currentTimerCount

    ## Gets the difference between the last position and the new position
    #
    #  In most cases this method subtracts the current timer count (encoder position) from the last known
    #  count. However, to account for overflow and underflow it first checks to see if the values are very
    #  far apart. If they are, it determines the direction of the over/underflow and procedes appropriately.
    #
    #  @param pastCount The last known timer count (encoder position)
    #  @param newCount The current timer count (encoder position)
    #  @return An integer representing the difference between the last position and the new position.
    def get_delta(self, pastCount, newCount):
        # Underflow
        if (pastCount < 5000 and newCount > 60000):
            return -(pastCount + (65535 - newCount))
        # Overflow
        elif (pastCount > 60000 and newCount < 5000):
            return newCount + (65535 - pastCount)
        # Normal
        else:
            return newCount - pastCount

    ## Gets the encoder's position
    #
    #  The return value is the current recorded absolute position (e.g. 65535*2 if the motor has completed
    #  two full rotations forward, negative if rotating backwards). This value does not reset due to any overflow
    #  or underflow until the capacity for the position variable type (integer) is reached.
    #
    #  @return An integer representing the position of the encoder
    def get_position(self):
        return self.position

    ## Sets the encoder's position
    #
    #  A direct replacement of the old position with the new position is made. All other logic happens elsewhere.
    #  e.g. encoder.set_position(0) will zero out the encoder. encoder.get_position would then return 0.
    #
    #  @param newPosition An integer representing an absolute position of the encoder.
    #  @return void
    def set_position(self, newPosition):
        self.position = newPosition
        
if __name__ =='__main__':
        # The following code is a test program for the encoder class. Any
        # code within the if __name__ == '__main__' block will only run when the
        # script is executed as a standalone program. If the script is imported as
        # a module the code block will not run.
        import utime
    
        # Create the pin objects used for interfacing with the encoder driver
        pB6 = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
        pB7 = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
    
        # Create an encoder object passing in the pins and corresponding timer (4)
        encoder = Encoder(pB6, pB7, 4)
        
        count = 0
        while 1:
            count = count + 1
            
            # Only print position every 1000 times through the loop
            print(encoder.get_position(), encoder.lastTimerCount, encoder.timer.counter())
            encoder.update()
            
            utime.sleep_ms(10)

           