''' @file CLPCStepResponsePage.py

    @page CLPCStepResponsePage Proportional Controller Step Response Test
    
    @section Purpose
    Often the most difficult task is not writing the controller and debugging the code but tuning it –
    adjusting operating parameters such as Kp so that the controller pushes the motor hard enough
    to get the load to the correct place quickly and accurately, but not so hard as to cause excessive
    overshoot or instability. To assist with controller tuning, we often perform step response tests in
    which the setpoint is instantly changed from zero to some nonzero value and the response of the
    system is recorded. The response is usually plotted on a graph of motor position vs. time.
    
    The following tests were performed using CLProportionalControllerTest
    
    @section Overview
    The goal of tuning this type of controller is to get your system to respond as fast as possible without
    causing too much overshoot or system instability. The 'acceptable' overshoot is an engineering decision
    determined by the requirements of your system. Sometimes it is not acceptable to have any overshoot.
    In this case, I chose my acceptable overshoot to be 5%, or 1575.
    
    @subsection tuning_goals Goals:
    <b>Speed of response:</b> As fast as possible
    
    <b>Acceptable overshoot:</b> 5%
    
    @section tuning_process Tuning Process
    To begin tuning my controller, I selected a starting Kp in the lower range of what I considered to be
    reasonable so as to avoid overestimating it. Knowing that my setpoint was 1500 encoder counts (just over
    two full rotations of the output shaft), the first error signal to be calculated by the controller
    after the step would be 1500. To transform this into a reasonable duty cycle between 0 and 100, a multiplyer
    of 0.02 would result in a duty cycle of 30. This seemed like a good starting place. 
    
    <b>Starting Kp:</b> 0.02
    
    @image html steptest1.png
    
    The set point is represented by the blue line, initially at 0 before stepping up to 1500 at 1 second.
    The red line represents the duty cycle provided to the motor by the proportional controller. You can see
    that it peaks at 30%, just at expected. The orange curve describes the response of the motor to the
    given duty cycles. This plot shows that not only is the response fairly slow, but it doesn't even get
    very close to the desired position within the test time. Luckily, with the duty cycle only reaching
    30% of it's potential, there is a lot of room to improve. Next, I decided to find the Kp that would cause
    the duty cycle to peak at 100%. Using basic proportions, this came out to be 0.0666.
    
    <b>Next Kp:</b> 0.0666
    
    @image html steptest3.png
    
    This time the duty cycle reaches its maximum value of 100%. As you can see, the response is much faster
    this time. However, it still doesn't quite reach the desired value, so I decide to increase it even further
    to 0.15 to see what happens.
    
    <b>Next Kp:</b> 0.15
    
    @image html steptest4.png
    
    This time the reponse does reach the target position eventually. It is also the first time we see the duty
    cycle requested by the controller go above 100% (Note the change in the right virtical axis). Now, surpassing
    100% duty cycle doesn't make any sense in the physical world, so during that time the motor is actually
    running at 100%. Therefore there is a new line, colored yellow, that represents the actual duty cycle of the
    motor. While it cannot go above 100%, the extra time it spends at its max value helps the motor to reach its
    final position faster. Also note that this is the first time the duty cycle dips below 0% as the motor very
    slightly overshoots the target and needs to be brought back. This response is good, but since we can afford
    some overshoot I decided to double it again and see what happens.
    
    <b>Next Kp:</b> 0.3
    
    @image html steptest5.png
    
    This time the requested duty cycle is massive (although, again, the actualy dutyc cycle of the motor saturates
    at 100%) and the motor actually overshoots the target position this time. In response, the dusty cycle dips
    below zero to correct the position. The position peaked at 1575, which is a 5% overshoot. This is exaclty the
    amount that was considered the maximum acceptable, meaning we have found the optimal Kp for the stated goals.
    
    @section tuning_conclusion Conclusion
    After experimenting with different values, a value of 0.3 resulted in a response with 5% overshoot,
    meaning it is the optimal value to achieve the fasting response while staying within the stated limits.
    However, if circumstances resulted in overshoot not being acceptable, it was shown in test 3 that a
    Kp of 0.15 would be a very good choice.
    
    @section tuning_video Step Response Video
    @htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/t11A4M6N-yY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    @endhtmlonly
    '''
