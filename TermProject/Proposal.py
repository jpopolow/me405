'''' @file Proposal.py

    @page term_project_proposal Project Proposal

    @section problem_statement Problem Statement
    I have always wanted to own a record player, and what's even more appealing is to own one
    that I've made myself. This project will entail creating a record player from scratch including the
    internal electronics.
    
    @section term_project_overview Overview
    This project is one I have wanted to do for a while. Limited by my knowledge of electronics I was only
    going to attempt to make the body and tranfer all of the electronics from an old record player.
    However, now that I have the knowledge from ME405, I feel confident that I can do much of the internal
    electronics myself as well.
    
    The turntable itself will be run by one of our motors in velocity control. This is the easy part.
    The needle arm assembly, taken from an old record player, comes with a proximity sensor to detrmine
    when it is at the limits of its motion. It will be upgraded with the help of additional motors in
    closed loop control to become autonomous, allowing it to automatically position itself to start and
    stop playing music. The needle will then pick up the information from the record and relay it to the
    included speaker.
    
    In my opinion this is a fairly ambitious project to be completed in 3 weeks, especially because it has
    significant hardware and software components, each of which will require at least some materials and
    knowledge that I do not currently posess. However, if I could get at least the electronics and software
    working in the next three weeks, I would be very happy.
    
    @section requirement_complience Requirement Complience
    Requirement                                  | Project Component
    -------------------------------------------- | -----------------------------------------------------
    Closed loop control on at least 2 actuators  | 2-3 motors in closed loop control 
    At least 2 sensors  | 2-3 encoders, 1 proximity sensor, 1 record stylus
    One new sensor | A proximity sensor and record stylus
    Interact with environment | Reading a record and playing music
    Implement using Nucleo hardware and MycroPython | These will by used to interface with all devices
    Run in real time | Nucleo will be reading multiple sensors and controlling mulitple actuators at once
    
    @section term_project_materials Materials
    @subsection Electronics
    Component | Acquired?
    --------- | ---------
    Nucleo | Yes
    2-3 motors with encoders | Might need 1 more
    1 proximity sensor | Yes
    1 record stylus | Yes
    1 speaker | Yes
    1 on/off switch | Yes
    1 rotary potentiometer (Volume knob) | Yes
    1 record | No
    
    @subsection Construction
    Material | Acquired?
    -------- | ---------
    Wood for main body | Yes
    Wood for turntable | No
    Wood glue | Yes
    Aluminum bar | No
    Aluminum tube | No
    
    @section assembly_plan Manufacturing and Assembly Plan
    I have wire, screwdrivers, a multimeter, and almost all of my actuators/sensors.
    I also already have an old record player ready to be taken apart, so I should be fine as far as materials
    and tools for the electronics. However, the physical body of the record player is more of a challenge.
    To be done efficiently I would need access to a number of tools I don't have such as clamps, a welder, and
    a bandsaw or something similar. I would either need to acquire these things or figure out how to do it without
    them, which may be difficult and more time consuming. This is why I am not expecting to finish the body during
    this project, though if I could that would be awesome.
    
    I would start with the electronics. Getting the turntable motor set up should be reletively simple, both
    positioning and programming. The stylus arm will be the bigger motor challenge in terms of physically setting
    up the mechanism as well as tuning its program to be accurate and precise. Then comes the part that I am unsure
    about. I will have to see if it is possible to run the stylus output/volume adjustment/speaker assumbly through
    the nucleo. Hopfully I can, at which point it will only be a matter of making sure all the parts work together
    and getting them fixed properly to the old record player framework that will be attached to the inside of the
    new body I will build.
    
    @section term_project_safety Safety
    This project will include rotating and moving parts, so appropriate caution of pinch points will be observed.
    The stylus needle is pointy and fragile so don't touch that. There shouldn't be any temperature or chemical
    concerns. Noise could potentially be an issue while working with the speaker, so appropriate caution will be
    observed then as well. During construction of the physical body if I get that far there will be use of power
    tools, saws, etc. During these activities I will be wearing appropriate PPE including safety glasses.
    
    @section Timeline
    I'm not really sure how a lot of this is going to go, so it's hard to provide a solid timeline, but this is a
    preliminary plan:
    @li Friday 5/22: Project proposal due
    @li Sunday 5/24: Complete preliminary design of record player while waiting for proposal feedback
    @li Friday 5/29: Finish taking apart old record player. Research interfacing with new sensors. Begin writing software
    @li Sunday 5/31: Start experimenting with hardware and software together
    @li Friday 6/5: Finish writing software. Start integrating hardware
    @li Sunday 6/7: Finish integrating hardware. Start tuning software
    @li Wednesday: 6/10: Complete project
    @li Friday 6/12: Complete report and demonstrate
'''