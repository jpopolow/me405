''' @file ProximityDriver.py

    @package ProximityDriver
    
    @subsection purpose Purpose
    This  driver class encapsulates all of the features
    associated  with  reading an analog proximity sensor
  
    @subsection background Background
    Proximity sensors a simply devices that idicated whether something is close to them or not. This
    one is a physical proximity sensor, meaning that contact with another opbject opens or closes a circuit,
    which can be read by our Nucleo as a binary signal. This sensor is active low, meaning that when something
    interacts with it the circuit is open and otherwise it is closed.
  
    @subsection impl Implementation Strategy
    Reading this type of sensor is quite simple. All we have to do is read the pin it is connected to. If it is high,
    the sensor is not being actuated, and if it is low it is.
  
    @subsection usage Usage
    To initialize a ProximityDriver object, simply supply its constructor the input pin that it is connected to. \n
    @code{.py}
    sensor = ProximitySensor('A9')
    @endcode
    
    Then, you can call methods on it. 
    
    @code{.py}
    sensor.isClose()
    sensor.isFar()
    @endcode
    
    Both return booleans
    
    @subsection testing Testing
    This class has been tested with the A9 pin on the Nucleo. Only isClose() has explicitly been tested
  
    @subsection bugs_and_lims Bugs and Limitations
    No major bugs, however it is know that if it is actuated slowly or the sampling period is too fast,
    it can return varying results as the sensor is actuated. Therefore, it is best to only sample it at a
    frequency that allows full actuation to be completed between samples.
  
    @subsection source Source Code
    https://bitbucket.org/jpopolow/me405/src/master/TermProject/ProximityDriver.py
  
    @author James Popolow

    @date June 12, 2020 '''


import pyb
import utime

class ProximityDriver:
    '''
    @class
    @brief Proximity sensor driver class
    @details Encapsulates all the functionality to read from a proximity sensor connected to an arbitrary pin
    @b Example:
    @code{.py}
    sensor = ProximityDriver('A9')

    while 1:
        print(sensor.isClose())
        utime.sleep_ms(100)
    @endcode
    '''
    
    ## Constructor for proximity sensor driver
    #
    #  This constructor takes a single pin representing the corresponding pin the sensor is connected to.
    #  The pin name can be specified as a string (e.g. 'A9') or an entire pin object can be provided (e.g. pyb.Pin.cpu.A9)
    #
    #  @param pin The pin corresponding to the sensor (e.g. pyb.Pin.cpu.A9 or 'A9')
    #  @return A ProximityDriver object
    def __init__(self, pin):
        ## The pin corresponding the the sensor
        self.pin = pyb.Pin(pin, pyb.Pin.PULL_DOWN)
        
    ## Returns whether or not sensor is actuated
    #
    #  Returns true if the sensor is actuated and false if it is not
    #
    #  @return boolean representing actuation of sensor
    def isClose(self):
        return not self.pin.value()
    
    ## Returns whether or not sensor is actuated
    #
    #  Returns false if the sensor is actuated and true if it is not
    #
    #  @return boolean representing actuation of sensor
    def isFar(self):
        return self.pin.value()