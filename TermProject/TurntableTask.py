'''
@file       TurntableTask.py
@brief      Task for operating the turntable motor
@details    This file handles the operation of the motor that turns the turntable.
            It uses the information from the proximity sensor on the needle arm
            to switch between states of Stopped, Playing, Done Playing, and Returning.
            The motor turns
            
            For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/TurntableTask.py
            
@page       page_turntable_task Turntable Task

@brief      Turntable Task Documentation

@details

@section    sec_turntable_task_intro Introduction
            This file handles the operation of the motor that turns the turntable.
            It uses the information from the proximity sensor on the needle arm
            to switch between states of Stopped, Playing, Done Playing, and Returning.
            The motor turns 

@section    sec_turntable_task_fsm Finite State Machine
            This task is implemented using a finite state machine with four
            states:
            @li Stopped
            @li Playing
            @li Done Playing
            @li Returning
@subsection sec_turntable_task_trans State Transition Diagram
@image      html TurntableTaskFSM.png width=600px

@section    sec_turntable_task_imp Implementation
            This task is implemented by the @ref TurntableTask class.
            
            For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/TurntableTask.py
'''


from MotorDriver import MotorDriver
from Encoder import Encoder
from ProximityDriver import ProximityDriver
from CLProportionalController import CLProportionalController


class TurntableStates:
    '''
    @class TurntableStates
    @brief      States for the Turntable Task
    @details    Simply a list of states for the turntable task to allow for more readable code.
                
                For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/TurntableTask.py
    '''
    STOPPED = 1
    PLAYING = 2
    DONE_PLAYING = 3
    RETURNING = 4 


class TurntableTask:
    '''
    @class TurntableTask
    @brief      Turntable Task
    @details    This task operates the turntable motor. It sets the motor to spin at a given desired speed
                using a closed loop proportional controller that reads the actual speed fo the motor using
                its encoder. This task uses the proximity sensor (limit sensor) to stop spinning the motor
                whenever the needle arm is in the reset position.
                
                For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/TurntableTask.py
    '''
        
    def __init__(self, turntableMotor: MotorDriver, motorEncoder: Encoder, limitSensor: ProximityDriver):
        '''
        @brief      Constructor for Turntable Task
        @details    This constructor initializes the objects and constants that will be needed by the individual states
        
        @param turntableMotor A MotorDriver object for interactig with the turntable motor
        @param motorEncoder An Encoder object for interacting with the turntable motor encoder.
        @param limitSensor A ProximityDriver object for interacting with the needle arm position sensor
        @return A TurntableTask object
        '''
        ## @brief   The turntable motor
        #  @details A MotorDriver object for the motor that turns the turntable
        self.motor = turntableMotor
        ## @brief   The turntable motor encoder
        #  @details An Encoder object for the motor that turns the turntable used to determine the speed of the motor
        self.encoder = motorEncoder
        ## @brief   The needle arm limit sensor
        #  @details A ProximityDriver object used to determine when to change states
        self.limitSensor = limitSensor
        ## @brief   Turntable motor closed loop controller
        #  @details A CLProportionalController used to run the turntable motor at the desired given speed.
        self.controller = CLProportionalController(1)
        ## @brief   Initializing the state of the task to Stopped
        #  @details Initialize state of task to Stopped so that motor does not run when device is first turned on.
        self.state = TurntableStates.STOPPED
        print("Changing turntable state to STOPPED")
        ## @brief   Desired speed for turntable motor
        #  @details Speed for motor tuned to 150
        self.speed = 150
        print('Created Task 1 Object')

        
    def run(self):
        '''
        @brief      Turntable Task run function
        @details    This method executes one single iteration of the turntable task. It is
                    modelled as a finite state machine, so based on the state of the task this
                    function will run the corresponding state function.
        '''
        
        if self.state == TurntableStates.STOPPED:
            self.stopped()
        elif self.state == TurntableStates.PLAYING:
            self.playing()
        elif self.state == TurntableStates.DONE_PLAYING:
            self.donePlaying()
        elif self.state == TurntableStates.RETURNING:
            self.returning()
        else:
            print("ERROR: Invalid turntable task state ", self.state)
            
    def stopped(self):
        '''
        @brief      Turntable task Stopped state function
        @details    This function is run when the task is in the Stopped state. It waits for the needle
                    arm to move away from the reset position and then enables the motor and sets the next state to Playing
        '''
        if not self.isArmAtLimit():
            self.motor.enable()
            self.state = TurntableStates.PLAYING
            print("Changing turntable state to PLAYING")
            
    def playing(self):
        '''
        @brief      Turntable task Playing state function
        @details    This function is run when the task is in the Playing state. It spins the motor by calling the spin function.
                    When the needle arm hits the fully extended position, it changes the next state to Done Playing
        '''
        self.spin()
        if self.isArmAtLimit():
            self.state = TurntableStates.DONE_PLAYING
            print("Changing turntable state to DONE_PLAYING")
            
    def donePlaying(self):
        '''
        @brief      Turntable Task Done Playing state function
        @details    This function is run when the task is in the Done Playing state. It spins the motor by calling the spin function.
                    When the needle arm leaves the fully extended position, it changes the next state to Returning
        '''
        self.spin()
        if not self.isArmAtLimit():
            self.state = TurntableStates.RETURNING
            print("Changing turntable state to RETURNING")
            
    def returning(self):
        '''
        @brief      Turntable Task Returning state function
        @details    This function is run when the task is in the Returning state. It spins the motor by calling the spin function.
                    When the needle arm reaches the reset position, it disables the motor and changes the next state to Stopped
        '''
        self.spin()
        if self.isArmAtLimit():
            self.motor.set_duty(0)
            self.motor.disable()
            self.state = TurntableStates.STOPPED
            print("Changing turntable state to STOPPED")
            
    def isArmAtLimit(self):
        '''
        @brief      Wrapper function for ProximityDriver
        @details    Wraps the Proximity driver to make other code in this class more readable. Calls and returns @code isFar() @endcode on the ProximityDriver
        '''
        return self.limitSensor.isFar()
    
    def spin(self):
        '''
        @brief      Function for spinning turntable motor
        @details    This function updates the encoder, then sends the desired operating speed and the actual speed of the motor (acquired
                    from the encoder) to the CLProportionalController which returns a duty cycle to be sent to the motor.
        '''
        self.encoder.update()
        self.motor.set_duty(self.controller.update(self.speed, self.encoder.get_speed()))
        print("Speed: ", self.speed, " Current Speed: ", self.encoder.get_speed(), " Duty: ", self.controller.update(self.speed, self.encoder.get_speed()))
