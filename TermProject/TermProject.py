''' @file TermProject.py

    @page term_project Term Project
    
    @section term_project_requirements Requirements
    Your project must include closed-loop control on at least two actuators. These could be the
    DC motors in your kit, or other actuators of your selection.
    @li Your project must interface with at least two sensors. You may use your IMUs and encoders
    if you desire.
    @li Your project must include at least one of the following sub-requirements:
    1. One new sensor that we have not covered in lab.
    2. One new actuator that we have not covered in lab.
    3. An especially novel or interesting software goal or feature that clearly improves the
    scope of your project.
    @li Your project must interact with the environment or a user in some non-trivial manner.
    @li You will implement your project using the Nucleo hardware running MicroPython.
    @li Your software must run in real time on the embedded hardware. This means multi-tasking.
    
    For information about the project see:
    - @subpage term_project_proposal
    - @subpage term_project_final_product
        
'''
