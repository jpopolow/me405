var annotated_dup =
[
    [ "CLProportionalController", "namespaceCLProportionalController.html", "namespaceCLProportionalController" ],
    [ "ElevatorTask", null, [
      [ "ElevatorStates", "classElevatorTask_1_1ElevatorStates.html", null ],
      [ "ElevatorTask", "classElevatorTask_1_1ElevatorTask.html", "classElevatorTask_1_1ElevatorTask" ]
    ] ],
    [ "Encoder", "namespaceEncoder.html", "namespaceEncoder" ],
    [ "IMUDriver", "namespaceIMUDriver.html", "namespaceIMUDriver" ],
    [ "MotorDriver", "namespaceMotorDriver.html", "namespaceMotorDriver" ],
    [ "ProximityDriver", "namespaceProximityDriver.html", "namespaceProximityDriver" ],
    [ "TurntableTask", null, [
      [ "TurntableStates", "classTurntableTask_1_1TurntableStates.html", null ],
      [ "TurntableTask", "classTurntableTask_1_1TurntableTask.html", "classTurntableTask_1_1TurntableTask" ]
    ] ],
    [ "ElevatorTask", "classElevatorTask.html", null ],
    [ "TurntableTask", "classTurntableTask.html", null ]
];