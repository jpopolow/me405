var main_8py =
[
    [ "encoderPin1", "main_8py.html#a10166cf05f177fd12bfd0624967f5e63", null ],
    [ "encoderPin2", "main_8py.html#a38276b7bcea6b7abbc901d6432a3539e", null ],
    [ "interval", "main_8py.html#a86060447ac22c1b649ef0497e8adf1ba", null ],
    [ "limitSensor", "main_8py.html#aab7f54634d8ca4929d33c762707e583e", null ],
    [ "motorEncoder", "main_8py.html#a195964d124d9fd470426bd4921f11b39", null ],
    [ "task_list", "main_8py.html#abd598416fd1a642cba229bb38b8c4846", null ],
    [ "turntableEnPin", "main_8py.html#a4613d5740c20f908f04298978083dc7f", null ],
    [ "turntableIn1", "main_8py.html#ab9c91b6a7f500d90e7f67120d73563c0", null ],
    [ "turntableIn2", "main_8py.html#ac874fd07cc5d738f9f902a4c0f529f48", null ],
    [ "turntableMotor", "main_8py.html#a9c25a40fe1f943e5bd3b8d4a70daf511", null ],
    [ "turntableTask", "main_8py.html#a2f1f619f7751e66419e4d65d1e9b2bf1", null ],
    [ "turntableTimer", "main_8py.html#ad0d67f54eb981be2bea9862dbc54b59c", null ]
];