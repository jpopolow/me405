var classTurntableTask_1_1TurntableTask =
[
    [ "__init__", "classTurntableTask_1_1TurntableTask.html#a0eb58938c49073160b87b73e5c3f1e8b", null ],
    [ "donePlaying", "classTurntableTask_1_1TurntableTask.html#a0ce857296568ad99481b2dd6f2486955", null ],
    [ "isArmAtLimit", "classTurntableTask_1_1TurntableTask.html#aa21abb5797f85eab101943a362e79fb5", null ],
    [ "playing", "classTurntableTask_1_1TurntableTask.html#ab2a429b651a6676f39e7ab0a4606f608", null ],
    [ "returning", "classTurntableTask_1_1TurntableTask.html#a78f08f914ef17b3976b852de7b135154", null ],
    [ "run", "classTurntableTask_1_1TurntableTask.html#aec65d83dcbe975b4f74f87a4ab35ccdc", null ],
    [ "spin", "classTurntableTask_1_1TurntableTask.html#a4ac7eac21bbdadde0ba01af1abeb1aad", null ],
    [ "stopped", "classTurntableTask_1_1TurntableTask.html#acf222bcbe6e18992812ec9ba4782c72f", null ],
    [ "controller", "classTurntableTask_1_1TurntableTask.html#aabf83a8177a0a1a7de737db9dd302f2f", null ],
    [ "encoder", "classTurntableTask_1_1TurntableTask.html#abd3b8f4246abe510cdb0b81cc1332b80", null ],
    [ "limitSensor", "classTurntableTask_1_1TurntableTask.html#aefc98c3704f27ba3c28ba2cd0a8aee57", null ],
    [ "motor", "classTurntableTask_1_1TurntableTask.html#a7102050c5e2569123aa773d85069a1ee", null ],
    [ "speed", "classTurntableTask_1_1TurntableTask.html#a6eaf9e485e0f00e01bd491286337f66e", null ],
    [ "state", "classTurntableTask_1_1TurntableTask.html#a8ff69a895a2c8e455a9e55cc16448048", null ]
];