var searchData=
[
  ['turntable_20task_71',['Turntable Task',['../page_turntable_task.html',1,'record_player_firmware']]],
  ['task_5flist_72',['task_list',['../main_8py.html#abd598416fd1a642cba229bb38b8c4846',1,'main']]],
  ['term_20project_73',['Term Project',['../term_project.html',1,'']]],
  ['termproject_2epy_74',['TermProject.py',['../TermProject_8py.html',1,'']]],
  ['timer_75',['timer',['../classEncoder_1_1Encoder.html#addfe70c4e55dff4194b850d804ca9e76',1,'Encoder::Encoder']]],
  ['turntableenpin_76',['turntableEnPin',['../main_8py.html#a4613d5740c20f908f04298978083dc7f',1,'main']]],
  ['turntablein1_77',['turntableIn1',['../main_8py.html#ab9c91b6a7f500d90e7f67120d73563c0',1,'main']]],
  ['turntablein2_78',['turntableIn2',['../main_8py.html#ac874fd07cc5d738f9f902a4c0f529f48',1,'main']]],
  ['turntablemotor_79',['turntableMotor',['../main_8py.html#a9c25a40fe1f943e5bd3b8d4a70daf511',1,'main']]],
  ['turntablestates_80',['TurntableStates',['../classTurntableTask_1_1TurntableStates.html',1,'TurntableTask']]],
  ['turntabletask_81',['TurntableTask',['../classTurntableTask.html',1,'TurntableTask'],['../classTurntableTask_1_1TurntableTask.html',1,'TurntableTask.TurntableTask'],['../main_8py.html#a2f1f619f7751e66419e4d65d1e9b2bf1',1,'main.turntableTask()']]],
  ['turntabletask_2epy_82',['TurntableTask.py',['../TurntableTask_8py.html',1,'']]],
  ['turntabletimer_83',['turntableTimer',['../main_8py.html#ad0d67f54eb981be2bea9862dbc54b59c',1,'main']]]
];
