var record_player_firmware =
[
    [ "Main Program", "record_player_firmware.html#record_player_firmware_main", null ],
    [ "Individual Tasks", "record_player_firmware.html#record_player_firmware_tasks", null ],
    [ "Source Code", "record_player_firmware.html#record_player_firmware_source", null ],
    [ "Turntable Task", "page_turntable_task.html", [
      [ "Introduction", "page_turntable_task.html#sec_turntable_task_intro", null ],
      [ "Finite State Machine", "page_turntable_task.html#sec_turntable_task_fsm", [
        [ "State Transition Diagram", "page_turntable_task.html#sec_turntable_task_trans", null ]
      ] ],
      [ "Implementation", "page_turntable_task.html#sec_turntable_task_imp", null ]
    ] ],
    [ "Elevator Task", "page_elevator_task.html", [
      [ "Introduction", "page_elevator_task.html#sec_elevator_task_intro", null ],
      [ "Finite State Machine", "page_elevator_task.html#sec_elevator_task_fsm", [
        [ "State Transition Diagram", "page_elevator_task.html#sec_elevator_task_trans", null ]
      ] ],
      [ "Implementation", "page_elevator_task.html#sec_elevator_task_imp", null ]
    ] ]
];