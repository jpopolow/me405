''' @file IMUDriver.py

    @package IMUDriver

    @subsection purpose Purpose
    This imu driver class simplifies interaction with a Bosch BNO055 9-axis IMU using I2C protocol.
  
    @subsection background Background
    (From the Lab 4 handout) \n
    An Inertial Measurement Unit is a sensing device used to determine the orientation of physical
    objects, typically with respect to the earth. In recent years these devices are increasingly common
    as mobile devices like smart phones require orientation sensing; how else could your phone autorotate?
    
    Two common varieties of IMU are 6-axis and 9-axis IMUs. A 6-axis IMU is composed of a 3-axis
    accelerometer, which measures linear acceleration in x, y, and z directions, and a 3-axis gyroscope,
    which measures angular velocity about x, y, and z directions. With some clever mathematics, these
    six pieces of information can be fused together to get a very accurate measurement of the pitch
    and roll of an object in earth’s gravity. A 9-axis IMU additionally includes a 3-axis magnetometer
    which measures magnetic field strength in x, y, and z directions to use as a compass. With this
    additional data the orientation can be further resolved to determine the heading of the object in
    addition to pitch and roll.
  
    @subsection impl Implementation Strategy
    IMUs come in both analog and digital interfaces, but modern low-cost IMUs almost always use a
    simple serial interface for communication. One very popular interface is the Inter-Integrated
    Circuit protocol, typically referred to as IIC or I2C. This protocol is convenient for interfacing between
    microcontrollers and sensors because it uses a limited number of wires and allows individual addressing of
    many sensors all connected to one main device. An I2C bus only requires two signal
    lines - data referred to as SDA and clock referred to as SCL.
  
    @subsection usage Usage
    To initialize an IMUDriver object, simply supply its constructor with the bus corresponding to the SDA and SCL
    pins that the sensor device has been hooked up to. \n
    @code{.py}
    imu = IMUDriver(1)
    @endcode
    
    Then, you can call methods on it. Generally you will want to enable it first.
    @code{.py} imu.enable() @endcode
    This method accepts a number representing an operating mode for the imu
    @code{.py} imu.enable(12) @endcode
    If you have already enabled the device and want to changle the mode, use setMode():
    @code{.py} imu.setMode(12) @endcode
    The operating mode is important, if your devie isn't outputting the data you want, double check it.
    
    Another thing to check is the calibration of the device sensors.
    @code{.py} getStatus @endcode will print the calibration status of each of the sensors,
    and return true if all are calibrated, false otherwise.
    
    You can also call @code{.py} getOrientation() @endcode and @code{.py} getAcceleration() @endcode on the imu.
    For example:
    
    @code{.py}
    imu = IMUDriver(1)
    imu.enable()
    imu.getStatus()
    print(imu.getOrientation(), " ", imu.getAngularVelocities())
    @endcode
    
    will print the calibration status to the console followed by lists representing the orientation and rotation speed,
    assuming the device has been connected properly and configured to the right mode.
    
    @subsection testing Testing
    This class has been tested by connecting the imu divice power, SDA and SCL outputs to the power, B7, and B8 pins
    of a nucleo. The imu device was then initialised and set to NDOF mode, and orientation and rotation speed was printed
    to the console every half second. Manual manipulation of the device and inspection of the data confirmed functionality
    of the driver.
    
    @htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/lRXYjvja0Cs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    @endhtmlonly
    
    @subsection bugs_and_lims Bugs and Limitations
    There are no known bugs in this driver, but there are several limitations.
    
    Firstly, this driver only allows limited read and write interactions with the imu. Currently, only
    orientation and rotation speed data can be retrieved. Also, only the operational mode can be changed by the user,
    no other configuration including output units have been exposed to user. Interaction with the IMU is severely limited by
    the current extent of impementation of this driver.
    
    Second, it has only been tested in NDOF mode. No other modes have been confirmed to be working.
    
    Finally, due to the limited nature of the driver implementation, gathering data other than angles and angular velocity was not tested.
  
    @subsection source Source Code
    https://bitbucket.org/jpopolow/me405/src/master/Lab4/IMUDriver.py
  
    @author James Popolow

    @date May 20, 2020 '''

import pyb
import utime
import ustruct

class IMUDriver:
    ''' Implements a driver for easily interacting with a Bosch BNO055 9-axis IMU using I2C protocol.

    @b Example
    @code
    if __name__ =='__main__':
    # The following code is a test program for the motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create IMU object and establish I2C connection to slave using bus 1
    imu = IMUDriver(1)
    
    # Enable IMU. Sets mode to NDOF by default
    imu.enable()
    # Or set it manually
    imu.enable(10)
    
    # To change mode later
    imu.setMode(12)
    
    # Check calibration statuses of system and sensors
    imu.checkStatus()
    
    # Retrieve and print Euler Angles and Angular Velocities
    operating = "true"
    while operating:
        print(imu.getOrientation(), " ", imu.getAngularVelocities())
        utime.sleep_ms(500)
        
    # Disable device once done
    imu.disable()
    @endcode

    Source code https://bitbucket.org/jpopolow/me405/src/master/Lab4/IMUDriver.py

    @author James Popolow
    @date May 20, 2020 '''
    
    # Global variables used for interacting with the IMU
    OPR_MODE_ADDR = 0x3D
    IMU_ADDR = 40
    ANGLE_ADDR = 0x1A
    VEL_ADDR = 0x14
    CALIB_STAT = 0x35
    UNIT_SEL_ADDR = 0x3B
    EUL_Unit = 4
    configMode = 0
    ndofMode = 12
    ANGLE_ADJUSTMENT = 16.0
    ANGULAR_VEL_ADJUSTMENT = 16.0
    ## Lambda used for dividing each element of a tuple by a given number
    adjust = lambda tup, denom : (tup[0] / denom, tup[1] / denom, tup[2] / denom)
    
    ## Constructor for the IMU Drvier
    #
    #  The constructor takes the bus that will be used for the I2C connection as its only argument.
    #  It then establishes the connection, assuming the physical device has been correctly connected to
    #  the pins corresponding to the given bus. It then enables the device, setting it to NDOF mode,
    #  and configures the output units for the sensors to m/s^2, degrees, and degrees per second.
    #  Finally, it breaks the connection with the deviec until furthur notice
    #
    #  @param bus Integer corresponding to the bus to be used for the I2C connection
    #  @return An IMUDriver object
    def __init__(self, bus):
        print("Initializing IMU")
        ## Integer representing the bus used for communnication with IMU
        self.bus = bus
        ## Object representing the I2C connection with the IMU
        self.i2c = pyb.I2C(bus)
        self.enable()
        self.i2c.mem_write(0b00010000, self.IMU_ADDR, self.UNIT_SEL_ADDR, timeout=1000)
        self.disable()
    
    ## Enables the IMU
    #
    #  Establishes the connection with the device and sets the given mode or NDOF if none is supplied
    #
    #  @param mode An optional integer specifying the desired imu operating mode (0-12). Defaults to NDOF mode (12)
    #  @return None
    def enable(self, mode=12):
        print("Enabling IMU")
        self.i2c = pyb.I2C(self.bus, pyb.I2C.MASTER)
        self.setMode(mode)
        utime.sleep_ms(700)
    
    ## Disables the IMU
    #
    #  Breaks the connection with the device
    def disable(self):
        print("Disable IMU")
        self.i2c.deinit()
    
    ## Sets the operating mode of the IMU
    #
    #  Changes the operating mode of the IMU to the given mode
    #
    #  @param mode Integer representing the desired operating mode for the IMU (0-12)
    def setMode(self, mode):
        print("Setting IMU to mode ", mode)
        self.i2c.mem_write(mode, self.IMU_ADDR, self.OPR_MODE_ADDR, timeout=1000)
        # Give IMU time to switch modes
        utime.sleep_ms(20)
    
    ## Prints device sensor calibration statuses
    #
    #  Prints the system, gyro, accelerometer, and magnetometer calibration statuses. If all are fully
    #  calibrated, returns true, otherwise returns false
    #
    #  @return boolean, true if all sensors are calibrated, false otherwise
    def checkStatus(self):
        statusArray = self.i2c.mem_read(1, self.IMU_ADDR, self.CALIB_STAT)
        statusString = bin((statusArray[0]))[2:]
        while len(statusString) < 8:
            statusString = "00" + statusString
        sysCal = statusString[0:2] == "11"
        gyrCal = statusString[2:4] == "11"
        accCal = statusString[4:6] == "11"
        magCal = statusString[6:8] == "11"
        print()
        print("Calibration Statuses:")
        print("  System:        ", sysCal)
        print("  Gyroscope:     ", gyrCal)
        print("  Accelerometer: ", accCal)
        print("  Magnetometer:  ", magCal)
        print()
        return sysCal and gyrCal and accCal and magCal
    
    ## Gets orientation as tuple of Euler angles
    #
    #  Retrieves Euler representation of angles from the device and returns them as a tuple (X, Y, Z) after dividing
    #  raw measurements by calibration constant of 16.
    #
    #  @return Tuple of Euler angles (in degrees by default) in the form (X, Y, Z)
    def getOrientation(self):
        angles = ustruct.unpack('<hhh', self.i2c.mem_read(6, self.IMU_ADDR, self.ANGLE_ADDR))
        adjustedAngles = tuple(angle/self.ANGLE_ADJUSTMENT for angle in angles)
        return adjustedAngles
    
    ## Gets angular velocities as tuple
    #
    #  Retrieves angular velocity data from the device and divides by the calibration constant of 16 before returning as a tuple
    #
    #  @param Tuple of angular accelerations (in degrees per second by default) in the form (X, Y, Z)
    def getAngularVelocities(self):
        angularVelocities = ustruct.unpack('<hhh', self.i2c.mem_read(6, self.IMU_ADDR, self.VEL_ADDR))
        adjustedVelocities = tuple(vel/self.ANGULAR_VEL_ADJUSTMENT for vel in angularVelocities)
        return adjustedVelocities
