''' @file FinalProductDocumentation.py

    @page term_project_final_product Final Product
    
    @section scope_and_completion Project Scope and Completion
    The @ref term_project_requirements for the project specified using cooperative
    multitasking and at least two sensors to control at least two actuators within a system that interacted with a user.
    My @ref term_project_proposal planned for the use of four sensors and 2-3 actuators in such a system to
    play music from a vinyl record. Due to delays in recieving parts, unforseen incompatabilities, and other factors I had
    to drop a sensor and actuator from the final product. So, the player I eded up with utilizes three sensors and one actuator
    in order to spin and stop the record automatically and read the music from it. 2 actuator if you are willing to count the speaker
    that plays the sound. And I did set up all the software for the missing components so that they will be very easy to add.
    
    While this didn't quite meet the goal I had planned for, I am very happy and proud of what I have acomplished. It is definitely not
    a perfect record player, but it is a prototype, a sort of proof-of-concept, that let me prove to myself that I could accomplish something
    like this. I plan to continue to work on this project in my own time to eventually bring it up to and beyond my original goals.
    
    @section term_project_demonstration Video Demonstration
    Again, its not a perfect record player. The motors are obnoxiously annoying and noisy and the speed is slightly under-tuned, but if you watch until the end you get to hear it stop!
    
    @htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/b0PJeULfF5Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    @endhtmlonly
    
    @section term_project_documentation Firmware Documentation
    For documentation of the software written for this project, see @ref record_player_firmware and for the source code visit https://bitbucket.org/jpopolow/me405/src/master/TermProject/
    '''