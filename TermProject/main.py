'''
@file       main.py
@brief      Main Script File
@details    Main script file that runs continuously. This task is reponsible for:
            @li Setting up device drivers
            @li Initializing tasks
            @li Running the task loop at a fixed interval.
            
            This could be considered
            a "round-robin" scheduler, which is a cooperative multi-tasking
            scheme.
            
            Each task is specified in a seperate file  using a Python 
            class. The class must contain a function called @c run that
            performs one single iteration of the task.
            
@page record_player_firmware Record Player Firmware Documentation

@section record_player_firmware_main Main Program
The main program is a simple script that sets up the drivers needed to interact with the devices

@code{.py}
# Set up the pins and timers needed by the tasks
turntableEnPin = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
turntableIn1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
turntableIn2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
turntableTimer = pyb.Timer(3, freq=20000);
encoderPin1 = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
encoderPin2 = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)

# Set up the devices
turntableMotor = MotorDriver(turntableEnPin, turntableIn2, turntableIn1, turntableTimer)
#elevatorMotor = MotorDriver('pins')
motorEncoder = Encoder(encoderPin1, encoderPin2, 4)
#elevatorEncoder = Encoder('pins')
limitSensor = ProximityDriver('A9')
@endcode

You can see that I set up the program to be ready to incorporate sensors and actuators for the needle arm,
but I had to leave them out of my final product.

Next, the main program sets up the tasks. Again, I set up tasks for both the turntable and the needle arm,
but only ended up including one of them.

@code{.py}
turntableTask = TurntableTask(turntableMotor, motorEncoder, limitSensor)
elevatorTask = ElevatorTask(limitSensor, elevatorMotor, elevatorEncoder)
task_list = [turntableTask', elevatorTask']
@endcode

Finally, it runs a continuous loop for which it calls the run() function within each of the tasks and then
waits 10 milliseconds to run again.

@code{.py}
interval = 10
while True:
    for task in task_list:
        task.run()
@endcode

@section record_player_firmware_tasks Individual Tasks

While only one of the tasks is actually running in my demo, I wrote and documented the second task so that it
would be easy to use later.

Tasks:
@li @subpage page_turntable_task "Turntable Task - Turning the record"
@li @subpage page_elevator_task "Elevator Task - Raising/lowering the needle arm"

@section record_player_firmware_source Source Code
https://bitbucket.org/jpopolow/me405/src/master/TermProject/main.py
https://bitbucket.org/jpopolow/me405/src/master/TermProject/
'''

import pyb
import utime
from MotorDriver import MotorDriver
from Encoder import Encoder
from ProximityDriver import ProximityDriver
# from elevator import ElevatorTask
from turntable import TurntableTask

## 
# @brief    Turntable motor enable pin
# @details  Enable pin for the turntable motor
#
turntableEnPin = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
## 
# @brief    Turntable motor pin 1
# @details  First IN pin for the turntable motor
#
turntableIn1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
## 
# @brief    Turntable motor pin 2
# @details  Second IN pin for the turntable motor
#
turntableIn2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);

## 
# @brief    Timer object for turntable motor
# @details  Timer object used for PWM generation
#
turntableTimer = pyb.Timer(3, freq=20000);

## 
# @brief    Turntable motor object
# @details  Responsible for driving the motor that turns the turntable
#
turntableMotor = MotorDriver(turntableEnPin, turntableIn2, turntableIn1, turntableTimer)
#elevatorMotor = MotorDriver('''pins''')

## 
# @brief    Encoder pin 1
# @details  First pin for the turntable motor encoder
#
encoderPin1 = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
## 
# @brief    Encoder pin 2
# @details  Second pin for the turntable motor encoder
#
encoderPin2 = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)

## 
# @brief    Turntable motor encoder
# @details  Encoder for the turntable motor. Used for speed control
#
motorEncoder = Encoder(encoderPin1, encoderPin2, 4)
#elevatorEncoder = Encoder('''pins''')

## 
# @brief    Needle arm proximity sensor
# @details  Detects when the needle arm is fully extended or fully retracted.
#           Used to determine when to turn on/off motors
#
limitSensor = ProximityDriver('A9')

## 
# @brief    Turntable Task
# @details  This is the task responsible for the operation of the motor that turns the turntable
#
turntableTask = TurntableTask(turntableMotor, motorEncoder, limitSensor)
# elevatorTask = ElevatorTask(limitSensor, elevatorMotor, elevatorEncoder)

## 
# @brief    List of task classes
# @details  This Python list contains all of the objects representing the
#           different tasks that will be called during the task loop.
#
task_list = [turntableTask]

##
# @brief    Time (in ms) between each loop through tasks
# @details  This value represents the number of milliseconds to wait before looping
#           through the tasks again. Since the tasks themselves take an essentially negligible
#           amount of time, this can be thought of as the time it takes to do one loop.
interval = 10

## @brief   Task loop rate in ms
#  @details A Python integer that represents the nominal number of microseconds
#           between iterations of the task loop.
#
while True:
    for task in task_list:
        task.run()
        
    utime.sleep_ms(interval)
    

