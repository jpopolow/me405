'''
@file       ElevatorTask.py
@brief      Elevator Task
@details    This task implements a finite state machine for controlling the motor that raises and
            lowers the needle arm. The motor has a lead screw attached to the shaft which, when
            spun, causes the wing nut on it to advance upwards or downwards, pushing on the needle
            arm along the way. It uses an Encoder together with a CLProportionalController to do
            so using position control, and a ProximityDriver to know when to change states
            
            For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/ElevatorTask.py
            
@page       page_elevator_task Elevator Task

@brief      Task 2 Documentation

@details

@section    sec_elevator_task_intro Introduction
            This task implements a finite state machine for controlling the motor that raises and
            lowers the needle arm. The motor has a lead screw attached to the shaft which, when
            spun, causes the wing nut on it to advance upwards or downwards, pushing on the needle
            arm along the way. It uses an Encoder together with a CLProportionalController to do
            so using position control, and a ProximityDriver to know when to change states

@section    sec_elevator_task_fsm Finite State Machine
            This task is implemented using a finite state machine with six
            states:
            @li Lowered
            @li Playing
            @li Raising
            @li Raised
            @li Returning
            @li Lowering
            
@subsection sec_elevator_task_trans State Transition Diagram
@image      html ElevatorTaskFSM.png width=600px

@section    sec_elevator_task_imp Implementation
            This task is implemented by the ElevatorTask class.
            
            For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/ElevatorTask.py
'''

import utime
import enum
from math import isclose
from ProximityDriver import ProximityDriver
from MotorDriver import MotorDriver
from Encoder import Encoder
from CLProportionalController import CLProportionalController

class ElevatorStates(Enum):
    '''
    @brief      States for the Elevator Task
    @details    Simply a list of states for the elevator task to allow for more readable code
                
                For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/ElevatorTask.py
    '''
    LOWERED = 1
    RAISING = 2
    RAISED = 3
    LOWERING = 4
    PLAYING = 5
    RETURNING = 6
    

class ElevatorTask:
    '''
    @class ElevatorTask
    @brief      Elevator Task
    @details    This class defines a task wich raises and lowers the needle arm of the record player.
                It uses a finite state machine to do so. During the raising and lowering states, a motor
                with a lead screw is turned in the appropriate direction using a CLProportionalController for
                position control to actuate the arm. The rest of the time it just waits for input from the
                proximity sensor to know when to move to the next state.
                    
                For source code see https://bitbucket.org/jpopolow/me405/src/master/TermProject/ElevatorTask.py
    '''
    
    def __init__(self, limitSensor: ProximityDriver, elevatorMotor: MotorDriver, elevatorEncoder: Encoder):
        '''
        @brief      Constructor for elevator task
        @details    This constructor initializes the objects and constants that will be needed by the individual states
        '''
        ## @brief   The needle arm limit sensor
        #  @details A ProximityDriver object used to determine when to change states
        self.limitSensor = limitSensor
        ## @brief   The elevator motor
        #  @details A MotorDriver object for the motor that elevates the needle arm
        self.elevator = elevatorMotor
        ## @brief   The elevator motor encoder
        #  @details An Encoder object for the motor that elevates the needle arm used to determine the position of the motor
        self.elevatorEnc = elevatorEncoder
        ## @brief   Elevator motor closed loop controller
        #  @details A CLProportionalController used to run the turntable motor to the desired given position.
        self.controller = CLProportionalController(0.15)
        ## @brief   Initializing the state of the task to Lowered
        #  @details Initialize state of task to Lowered so that it is in the correct starting state when the device is turned on.
        self.state = ElevatorStates.LOWERED
        ## @brief   Raised position
        #  @details A value representing the amount the motor must turn to get it to raise the needle arm to full hight.
        self.raisedPosition = 50000
        ## @brief   Lowered position
        #  @details A value representing the reset position of the motor allowing the needle to fall to resting height
        awlf.loweredPosition = 0
        print('Created Task 2 Object')

        
    def run(self):
        '''
        @brief      Turntable Task run function
        @details    This method executes one single iteration of the elevator task. It is
                    modelled as a finite state machine, so based on the state of the task this
                    function will run the corresponding state function.
        '''
        if self.state == ElevatorStates.LOWERED:
            self.lowered()
        elif self.state == ElevatorStates.RAISING:
            self.raising()
        elif self.state == ElevatorStates.RAISED:
            self.raised()
        elif self.state == ElevatorStates.LOWERING:
            self.lowering()
        elif self.state == ElevatorStates.PLAYING:
            self.playing()
        elif self.state == ElevatorStates.RETURNING:
            self.returning()
        else:
            print("ERROR: Invalid elevator task state ", self.state)
            
    def lowered(self):
        '''
        @brief      Elevator task Lowered state function
        @details    This function is run when the task is in the Lowered state. It waits for the needle
                    arm to move away from the reset position and then sets the next state to Playing
        '''
        if not isArmAtLimit():
            self.state = ElevatorStates.PLAYING
    
    def raising(self):
        '''
        @brief      Elevator task Raising state function
        @details    This function is run when the task is in the Raising state. It updates the encoder, waits for the needle
                    arm to get close enough to the raised position, then disables the motor and sets the next state to Raised
        '''
        elevatorEnc.update()
        self.elevator.set_duty(self.controller.update(self.raisedPosition, self.elevatorEnc.get_position()))
        if isClose(self.elevatorEnc.get_position(), self.raisedPosition, 0.02):
            self.elevator.set_duty(0)
            self.elevator.disable()
            self.state = ElevatorStates.RAISED
            
    
    def raised(self):
        '''
        @brief      Elevator task Raised state function
        @details    This function is run when the task is in the Raised state. It waits for the needle
                    arm to move away from the reset position and then sets the next state to Returning
        '''
        if not isArmAtLimit():
            self.state = ElevatorStates.RETURNING
    
    def lowering(self):
        '''
        @brief      Elevator task Lowering state function
        @details    This function is run when the task is in the Lowering state. It updates the encoder, waits for the needle
                    arm to get close enough to the lowered position, then disables the motor and sets the next state to Lowered
        '''
        elevatorEnc.update()
        self.elevator.set_duty(self.controller.update(self.loweredPosition, self.elevatorEnc.get_position()))
        if isClose(self.elevatorEnc.get_position(), self.loweredPosition, 0.02):
            self.elevator.set_duty(0)
            self.elevator.disable()
            self.state = ElevatorStates.LOWERED
    
    def playing(self):
        '''
        @brief      Elevator task Playing state function
        @details    This function is run when the task is in the Playing state. It waits for the needle
                    arm to move to the extended position and then sets the next state to Raising
        '''
        if(isArmAtLimit()):
            self.elevator.enable()
            self.state = ElevatorStates.RAISING
    
    def returning(self):
        '''
        @brief      Elevator task Returning state function
        @details    This function is run when the task is in the Returning state. It waits for the needle
                    arm to move to the reset position and then sets the next state to Lowering
        '''
        if(isArmAtLimit()):
            self.elevator.enable()
            self.state = ElevatorStates.LOWERING   
        
    def isArmAtLimit(self):
        return self.limitSensor.isFar()
    
    
