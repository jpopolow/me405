''' @file CLProportionalController.py
  
    @package CLProportionalController

    @subsection purpose Purpose
    This file implements a closed-loop proportional controller for the purpose of 
    manipulating our DC motors using position control. However, the implementation
    is generic, so it could be used to control other things (heaters, aircraft, engines,
    people, etc.)
    
    @subsection background Background
    DC motors with closed-loop control systems provide some of the fastest, most efficient, and most
    accurate systems available for moving things to specified positions. A motor with closed-loop
    control is often referred to as a servo system. If you have a motor, motor driver and position
    sensor just lying around, you can write code to operate the motor in closed-loop position servo
    mode. One algorithm is the proportional controller.
  
    @subsection impl Implementation Strategy
    Proportional-only controllers are fairly straightforward:
    \li Supply as an input the setpoint, the desired location of the motor. (This is refered to as the reference)
    \li Subtract the measured location of the motor (the System Output) from the setpoint; the difference is the Error
        Signal, a signed number indicating which way the motor is off and how far.
    \li Multiply the error signal by a control gain called Kp to produce a result called the Actuation
        Signal. The larger the error, the harder the controller will push.
    \li Send the actuation signal to an actuator, like a motor driver to control the magnitude and direction of motor torque.
    \li Other terms can be added based on the integral and derivative of the error, but these are not required for a proportional-only controller.
    
    The units of gains such as Kp can be weird. If position is measured by an encoder in units of
    encoder counts and the actuation signal is a duty cycle in percent, the units of Kp are percent
    duty cycle per encoder count.
  
    @subsection usage Usage
    Simple usage example:
    @code

    motor = # A motor object
    encoder = # An encoder object
    
    kp = # Kp value 
    
    controller = CLProportionalController(kp)
    
    setpoint = # Some desired position for the system
    
    while systemIsOn:
        encoder.update() # Let encoder get its current position
        newDuty = controller.update(setpoint, encoder.get_position())
        motor.set_duty(newDuty)
        utime.sleep_ms(10)
    @endcode
    
    For a more detailed example, see CLProportionalControllerTest
    
    @subsection testing Testing
    This controller was tested using a step response test. For the test program see
    CLProportionalControllerTest and for test results and more info see
    \ref CLPCStepResponsePage
  
    @subsection bugs_and_lims Bugs and Limitations
    \b Limitations:
    \li This controller is only capable of proportional-only control. Integral and derivative
        terms are not included.
    \li Performance of the controller is subject to the provided Kp. A Kp that is too big could
        result in excessive overshoot or system instability
        
        \b Bugs:
        li\ No known bugs
  
    @subsection source Source Code
    https://bitbucket.org/jpopolow/me405/src/master/Lab3/CLProportionalController.py
  
    @author James Popolow

    @date May 13, 2020 '''

import pyb

class CLProportionalController:
    ''' For a thorough description see CLProportionalController

    @b Example
    @code
    if __name__ =='__main__':
        # The following code is a test program for the encoder class. Any
        # code within the if __name__ == '__main__' block will only run when the
        # script is executed as a standalone program. If the script is imported as
        # a module the code block will not run.
    
        # Test code 
    @endcode 

    Source code https://bitbucket.org/jpopolow/me405/src/master/Lab3/CLProportionalController.py

    @author James Popolow
    @date May 13, 2020 '''
    
    ## Constructor for the controller
    #
    #  Takes a proportional gain Kp as a n input. This Kp may have varying and interesting units
    #  depending on the sytem and controller application. In the case of a DC motor, for example,
    #  the units may be duty cycle per encoder count. No additional setup required.
    #
    #  @param kp The numerical proportional gain for the controller.
    #  @return A Closed Loop Proportional Controller object
    def __init__(self, kp):
        print("Setting up a P controller with Kp = ", kp)
        ## Proportional gain for the controller
        self.kp = kp
    
    ## Update the controller
    #
    #  Accepts the system reference point (desired output) and the system output (measured system characteristic),
    #  calculates the difference between the two (the error signal), and multiplies by the provided gain Kp
    #  returning the new actuation signal for the system.
    #
    #  @param reference Desired numerical setpoint for the system. (e.g. desired position)
    #  @param systemOutput Measured system characteristic (e.g. actual position)
    #  @return New actuation signal for the system. (e.g. new duty cycle for motor)
    def update(self, reference, systemOutput):
        error = reference - systemOutput
        actuationSignal = error * self.kp
        return actuationSignal
